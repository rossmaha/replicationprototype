﻿using System;

namespace Replication.Infastructure.CommonUtils
{
    public static class TypeExtensions
    {
        public static string ToDataString(this Type type)
        {
            var typeArray = type.AssemblyQualifiedName.Split(" ".ToCharArray());
            var returnValue = typeArray[0] + " " + typeArray[1].Replace(",", "");
            return returnValue;
        }
    }
}
