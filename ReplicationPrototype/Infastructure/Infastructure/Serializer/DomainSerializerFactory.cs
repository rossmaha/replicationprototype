﻿namespace Replication.Infastructure.Serializer
{
    public class DomainSerializerFactory
    {
        public static IDomainSerializer CreateSerializer()
        {
            return new JsonDomainEventSerializer();
        }
    }
}
