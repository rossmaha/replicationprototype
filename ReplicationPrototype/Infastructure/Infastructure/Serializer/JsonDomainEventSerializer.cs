﻿using System;
using JsonNet.PrivateSettersContractResolvers;
using Newtonsoft.Json;

namespace Replication.Infastructure.Serializer
{
    internal class JsonDomainEventSerializer : IDomainSerializer
    {
        public string Serialize(object domainEvent)
        {
            return JsonConvert.SerializeObject(domainEvent);
        }

        public T Deserialize<T>(Type targetType, string serializedDomainObject)
        {
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new PrivateSetterContractResolver()
            };

            return (T) JsonConvert.DeserializeObject(serializedDomainObject, targetType, settings);
        }
    }
}
