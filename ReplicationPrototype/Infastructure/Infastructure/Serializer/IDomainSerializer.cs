﻿using System;

namespace Replication.Infastructure.Serializer
{
    public interface IDomainSerializer
    {
        string Serialize(object domainObject);

        T Deserialize<T>(Type targetType, string serializedDomainObject);
    }
}
