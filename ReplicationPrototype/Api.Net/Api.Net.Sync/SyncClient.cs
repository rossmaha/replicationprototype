﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using Core.Sync.Dto;

namespace Api.Net.Sync
{
    internal class SyncClient : ISyncClinet
    {
        private HttpClient _httpClient;
        private readonly Uri _baseAddress;
        private readonly TimeSpan _timeoutMillisec;

        public SyncClient(string baseAddress, string apiPrefix = "api", long timeoutMillisec = 5*60*1000)
        {
            _baseAddress = new Uri($"{baseAddress.TrimEnd('/')}/{apiPrefix}/");
            _timeoutMillisec = TimeSpan.FromMilliseconds(timeoutMillisec);
        }

        public IEnumerable<EventWithMetaDataDto> GetEventsToSync(IEnumerable<EventWithMetaDataDto> pastEvents)
        {
            var result = GetClient().PostAsJsonAsync(_baseAddress + "Sync/SyncEvents", pastEvents).Result;
            result.EnsureSuccessStatusCode();
            return result.Content.ReadAsAsync<IEnumerable<EventWithMetaDataDto>>().Result;
        }

        private HttpClient GetClient()
        {
            return _httpClient ?? (_httpClient = Create());
        }

        private HttpClient Create()
        {
            var client = HttpClientFactory.Create();
            client.BaseAddress = _baseAddress;
            client.Timeout = _timeoutMillisec;

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            return client;
        }
    }
}
