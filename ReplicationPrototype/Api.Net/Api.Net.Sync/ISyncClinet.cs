﻿using System.Collections.Generic;
using Core.Sync.Dto;

namespace Api.Net.Sync
{
    public interface ISyncClinet
    {
        IEnumerable<EventWithMetaDataDto> GetEventsToSync(IEnumerable<EventWithMetaDataDto> pastEvents);
    }
}
