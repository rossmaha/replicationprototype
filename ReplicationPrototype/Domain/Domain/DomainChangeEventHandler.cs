﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Replication.Domain.Models;
using Replication.Domain.Repositories;

namespace Replication.Domain
{
    public class DomainChangeEventHandler : Event.Models.DomainEventHandler<IncidentCreatedEvent>
    {
        private readonly IIncidentRepository _incidentRepository;

        public DomainChangeEventHandler(IUnitOfWork unitOfWork)
        {
            _incidentRepository = unitOfWork.Incidents;
        }

        public override void Handle(IncidentCreatedEvent handle)
        {
            _incidentRepository.Add(new Incident(handle.AggregateId, handle.Name)
            {
                Description = handle.Description,
            });
        }
    }
}
