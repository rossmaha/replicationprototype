﻿using System;
using Replication.Domain.Event.Models;

namespace Replication.Domain
{
    public interface IAggregateRoot
    {
        Guid Id { get; }

        DateTime UpdateTime { get; set; }

        void ApplyEvents(params DomainEvent[] eventHistory);
    }

    public abstract class AggregateRoot<TCreateEvent> : IAggregateRoot 
        where TCreateEvent : DomainEvent
    {
        protected AggregateRoot()
        {
            UpdateTime = DateTime.Now.ToUniversalTime();
        }

        public Guid Id { get; protected internal set; }

        public DateTime UpdateTime { get; set; }

        public void ApplyEvents(params DomainEvent[] eventHistory)
        {
            foreach (var eventToApply in eventHistory)
            {
                dynamic d = this;

                d.Handle(Convert.ChangeType((dynamic) eventToApply, eventToApply.GetType()));
                UpdateTime = eventToApply.EventTime;
            }
        }

        protected abstract void Handle(TCreateEvent createEvent);

        protected abstract void Handle(DomainChangeEvent changeEvent);
    }
}
