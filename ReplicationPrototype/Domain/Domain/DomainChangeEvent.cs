﻿using System;
using System.Collections.Generic;
using Replication.Domain.Event.Models;

namespace Replication.Domain
{
    public class DomainChangeEvent : DomainEvent
    {
        public DomainChangeEvent(Type aggregateType, Guid aggregateId, DateTime eventTime)
            : base(aggregateType, aggregateId, eventTime)
        {
            Dictionary = new Dictionary<string, object>();
        }

        //TODO 
        public Dictionary<string, object> Dictionary { get; private set; }
    }
}
