﻿using System;

namespace Replication.Domain.Models
{
    public class Ticket : AggregateRoot<TicketCreatedEvent>
    {
        public Ticket(Incident incident, string name, string description)
        {
            Incident = incident;
            IncidentId = incident.Id;

            Name = name;
            Description = description;
        }

        public virtual Incident Incident { get; private set; }

        public virtual Guid IncidentId { get; private set; }
        
        
        public string Name { get; private set; }

        public string Description { get; private set; }


        protected override void Handle(TicketCreatedEvent createEvent)
        {
            throw new NotImplementedException();
        }

        protected override void Handle(DomainChangeEvent changeEvent)
        {
            throw new NotImplementedException();
        }
    }
}
