﻿using System;
using Replication.Domain.Event.Models;

namespace Replication.Domain.Models
{
    public class TicketCreatedEvent : DomainEvent
    {
        public TicketCreatedEvent(Guid aggregateId, DateTime dateTime)
            : base(typeof(Ticket), aggregateId, dateTime)
        {
        }
    }
}
