﻿using Replication.Domain.Event.Models;
using Replication.Domain.Repositories;

namespace Replication.Domain.Models
{
    public class IncidentCreatedEventHandler : DomainEventHandler<IncidentCreatedEvent>
    {
        private readonly IIncidentRepository _incidentRepository;

        public IncidentCreatedEventHandler(IUnitOfWork unitOfWork)
        {
            _incidentRepository = unitOfWork.Incidents;
        }

        public override void Handle(IncidentCreatedEvent handle)
        {
            _incidentRepository.Add(new Incident(handle.AggregateId, handle.Name)
            {
                Description = handle.Description,
            });
        }
    }
}
