﻿using System;
using Replication.Domain.Event.Models;

namespace Replication.Domain.Models
{
    public class IncidentCreatedEvent : DomainEvent
    {
        public IncidentCreatedEvent(Guid aggregatId, string name, string description, DateTime eventTime)
            : base(typeof (Incident), aggregatId, eventTime)
        {
            Name = name;
            Description = description;
        }

        public string Name { get; private set; }

        public string Description { get; private set; }
    }
}
