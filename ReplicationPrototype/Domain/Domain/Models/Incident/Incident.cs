﻿using System;

namespace Replication.Domain.Models
{
    public class Incident : AggregateRoot<IncidentCreatedEvent>
    {
        private Incident()
        {
            
        }

        public Incident(Guid incidentId, string name) : this()
        {
            Id = incidentId;
            Name = name;
            //Tickets = new List<Ticket>();
        }

        public string Name { get; set; }

        public string Description { get; set; }


        protected override void Handle(IncidentCreatedEvent incidentCreatedEvent)
        {
            Id = incidentCreatedEvent.AggregateId;
            Name = incidentCreatedEvent.Name;
            Description = incidentCreatedEvent.Description;
        }

        protected override void Handle(DomainChangeEvent domainChangeEvent)
        {
            foreach (var property in domainChangeEvent.Dictionary)
            {
                switch (property.Key.ToLower())
                {
                    case "name":
                        Name = (string)property.Value;
                        break;
                    case "description":
                        Description = (string)property.Value;
                        break;
                }
            }
        }
    }
}
