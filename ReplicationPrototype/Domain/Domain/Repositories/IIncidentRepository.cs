﻿using System;
using Replication.Domain.Models;

namespace Replication.Domain.Repositories
{
    public interface IIncidentRepository
    {
        void Add(Incident incident);

        Incident GetById(Guid id);

        void Update(Incident incident);
    }
}
