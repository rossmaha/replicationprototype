﻿namespace Replication.Domain.Repositories
{
    public interface ISaveContext
    {
        void Save();
    }

    public interface IUnitOfWork : ISaveContext
    {
        IIncidentRepository Incidents { get; }
    }
}
