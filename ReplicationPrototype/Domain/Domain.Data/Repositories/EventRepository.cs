﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Replication.Domain.Data.Models;
using Replication.Domain.Event;
using Replication.Domain.Event.Models;

namespace Replication.Domain.Data.Repositories
{
    public class EventRepository : IEventRepository
    {
        private readonly ReplicationPrototypeContext _context;
        private readonly IDbSet<EventDal> _set;

        public EventRepository(ReplicationPrototypeContext contex)
        {
            _context = contex;
            _set = contex.Events;
        }

        public IEnumerable<DomainEventMetadata> GetEventsByAggreagateId(Guid id)
        {
            return _set.Where(e => e.AggregateId == id).Select(ConvertToEventWithMetaData).ToList();
        }

        public IEnumerable<DomainEventMetadata> GetEventsSinceDate(DateTime? lastUpDateTime)
        {
            if (lastUpDateTime.HasValue)
            {
                return _set.Where(e => e.DateTime > lastUpDateTime.Value).Select(ConvertToEventWithMetaData).ToList();
            }

            // TODO Забрать ВСЕ СОБЫТИЯ БАЗЫ?
            return _set.Select(ConvertToEventWithMetaData);
        }

        public void SaveChangeEvent(Type aggregateType, Guid aggregateId, DomainEventMetadata domainEvent)
        {
            var aggregate = GetAggregateByType(aggregateType, aggregateId);
            aggregate.ApplyEvents(domainEvent.GetDomainEvent());
        }

        private IAggregateRoot GetAggregateByType(Type type, Guid aggregateId)
        {
            return _context.Set(type).Cast<IAggregateRoot>().FirstOrDefault(r => r.Id == aggregateId);
        }


        private static DomainEventMetadata ConvertToEventWithMetaData(EventDal eventDal)
        {
            return new DomainEventMetadata(Type.GetType(eventDal.AggregateType), eventDal.AggregateId, eventDal.DateTime,
                Type.GetType(eventDal.EventType), eventDal.Data);
        }
    }
}
