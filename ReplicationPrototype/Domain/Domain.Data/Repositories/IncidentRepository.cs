﻿using System;
using System.Linq;
using Replication.Domain.Data.Repositories.Common;
using Replication.Domain.Models;
using Replication.Domain.Repositories;

namespace Replication.Domain.Data.Repositories
{
    public class IncidentRepository : StoreEventRepository, IIncidentRepository
    {
        private readonly ReplicationPrototypeContext _context;

        public IncidentRepository(ReplicationPrototypeContext context) :base(context)
        {
            _context = context;
        }

        public Incident GetById(Guid id)
        {
            return _context.Incidents.Single(i => i.Id == id);
        }

        public void Update(Incident incident)
        {
        }

        public void Add(Incident incident)
        {
            _context.Incidents.Add(incident);

            AddEvent(incident, new IncidentCreatedEvent(incident.Id, incident.Name, incident.Description, incident.UpdateTime));
        }
    }
}
