﻿using System;
using System.Data.Entity;
using Replication.Domain.Data.Models;
using Replication.Domain.Event.Models;
using Replication.Infastructure.CommonUtils;
using Replication.Infastructure.Serializer;

namespace Replication.Domain.Data.Repositories.Common
{
    public abstract class StoreEventRepository
    {
        private readonly IDomainSerializer _eventSerializer;
        private readonly IDbSet<EventDal> _eventSet;

        protected StoreEventRepository(ReplicationPrototypeContext context)
        {
            _eventSerializer = DomainSerializerFactory.CreateSerializer();
            _eventSet = context.Events;
        }

        protected void AddEvent<T>(T aggregateRoot, DomainEvent eventToAdd) where T : IAggregateRoot
        {
            _eventSet.Add(new EventDal
            {
                AggregateId = aggregateRoot.Id,
                AggregateType = typeof(T).ToDataString(),
                DateTime = eventToAdd.EventTime,
                Data = _eventSerializer.Serialize(eventToAdd) ?? String.Empty,
                EventType = eventToAdd.GetType().ToDataString()
            });
        }
    }
}
