﻿using System;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using Replication.Domain.Data.Models;
using Replication.Domain.Repositories;
using Replication.Infastructure.CommonUtils;
using Replication.Infastructure.Serializer;

namespace Replication.Domain.Data.Repositories.Common
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ReplicationPrototypeContext _context;
        private readonly IDomainSerializer _eventSerializer;

        public UnitOfWork(ReplicationPrototypeContext context)
        {
            _context = context;
            _eventSerializer = DomainSerializerFactory.CreateSerializer();

            Incidents = new IncidentRepository(_context);
        }

        public void Save()
        {
            var entries = _context.ChangeTracker.Entries().Where(c => c.State == EntityState.Modified).ToList();

            foreach (var entry in entries.Select(e=>e.Entity).OfType<IAggregateRoot>())
            {
                var originalValues = _context.Entry(entry).OriginalValues;
                var currentValues = _context.Entry(entry).CurrentValues;

                var eventToAdd = new DomainChangeEvent(entry.GetType(), entry.Id, DateTime.Now.ToUniversalTime());
                entry.UpdateTime = eventToAdd.EventTime;

                foreach (string propertyName in originalValues.PropertyNames)
                {
                    var original = originalValues[propertyName];
                    var current = currentValues[propertyName];

                    if (!Equals(original, current))
                    {
                        eventToAdd.Dictionary.Add(propertyName, current);
                    }
                }

                _context.Events.Add(new EventDal
                {
                    AggregateId = entry.Id,
                    AggregateType = entry.GetType().ToDataString(),
                    DateTime = entry.UpdateTime,
                    EventType = typeof(DomainChangeEvent).ToDataString(),
                    Data = _eventSerializer.Serialize(eventToAdd) ?? String.Empty,
                });
            }

            try
            {
                _context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    //Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                    //    eve.Entry.Entity.GetType().Name, eve.Entry.State);

                    foreach (var ve in eve.ValidationErrors)
                    {
                        var str = String.Format("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
        }

        public IIncidentRepository Incidents { get; }
    }
}
