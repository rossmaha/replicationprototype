﻿using System.Data.Entity;
using Replication.Domain.Data.Configurations;
using Replication.Domain.Data.Models;
using Replication.Domain.Models;

namespace Replication.Domain.Data
{
    public class ReplicationPrototypeContext : DbContext
    {
        public DbSet<Incident> Incidents { get; set; }

        public DbSet<EventDal> Events { get; set; }


        public ReplicationPrototypeContext() : base("ReplicationPrototypeConnection")
        {
        }

        public ReplicationPrototypeContext(string connectionName) : base(connectionName)
        {
        }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            RegisterConfigurations(modelBuilder);
        }

        private static void RegisterConfigurations(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new IncidentConfiguration());
        }
    }
}
