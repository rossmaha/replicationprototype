﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Replication.Domain.Models;

namespace Replication.Domain.Data.Configurations
{
    public class IncidentConfiguration : EntityTypeConfiguration<Incident>
    {
        public IncidentConfiguration()
        {
            ToTable("Incidents", "dbo");

            HasKey(i => i.Id);

            Property(i => i.Id)
                .HasColumnName("Id")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(i => i.Name)
                .HasColumnName("Name")
                .IsRequired()
                .HasMaxLength(150);

            Property(i => i.Description)
                .HasColumnName("Description")
                .HasMaxLength(300);

            Property(i => i.UpdateTime).HasColumnType("datetime2").IsRequired();
        }
    }
}
