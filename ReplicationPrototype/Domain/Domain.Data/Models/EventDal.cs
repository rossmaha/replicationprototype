﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Replication.Domain.Data.Models
{
    [Table("Events")]
    public class EventDal
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public Guid AggregateId { get; set; }

        [Required]
        [MaxLength(300)]
        public string AggregateType { get; set; }

        [Required]
        [Column(TypeName = "datetime2")]
        public DateTime DateTime { get; set; }

        [Required]
        [MaxLength(300)]
        public string EventType { get; set; }

        [Required]
        public string Data { get; set; }
    }
}
