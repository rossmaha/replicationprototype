﻿using Ninject;
using Ninject.Modules;
using Ninject.Web.Common;
using Replication.Domain.Data.Repositories;
using Replication.Domain.Data.Repositories.Common;
using Replication.Domain.Event;
using Replication.Domain.Repositories;

namespace Replication.Domain.Data
{
    public class DomainDataModule : NinjectModule
    {
        public override void Load()
        {
            Bind<ReplicationPrototypeContext>().ToSelf().InRequestScope();
            Bind<IUnitOfWork, ISaveContext>().To<UnitOfWork>().InRequestScope();
            Bind<IIncidentRepository>().ToMethod(ctx => ctx.Kernel.Get<IUnitOfWork>().Incidents);
            Bind<IEventRepository>().To<EventRepository>();
        }
    }
}
