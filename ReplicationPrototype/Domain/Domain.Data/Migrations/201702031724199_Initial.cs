using System.Data.Entity.Migrations;

namespace Replication.Domain.Data.Migrations
{
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Events",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        AggregateId = c.Guid(nullable: false),
                        AggregateType = c.String(nullable: false, maxLength: 300),
                        DateTime = c.DateTime(nullable: false),
                        EventType = c.String(nullable: false, maxLength: 300),
                        Data = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Incidents",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 150),
                        Description = c.String(maxLength: 300),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Incidents");
            DropTable("dbo.Events");
        }
    }
}
