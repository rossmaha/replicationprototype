namespace Replication.Domain.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUpdatetime : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Incidents", "UpdateTime", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Events", "DateTime", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Events", "DateTime", c => c.DateTime(nullable: false));
            DropColumn("dbo.Incidents", "UpdateTime");
        }
    }
}
