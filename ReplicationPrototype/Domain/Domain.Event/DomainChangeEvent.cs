﻿using System;
using System.Collections.Generic;
using Replication.Domain.Event.Models;

namespace Replication.Domain.Event
{
    public class DomainChangeEvent : DomainEvent
    {
        public Dictionary<string, object> Dictionary { get; set; }

        public DomainChangeEvent(Guid aggregateId, Type aggregateType, DateTime eventTime)
            : base(aggregateType, aggregateId, eventTime)
        {
            Dictionary = new Dictionary<string, object>();
        }
    }
}
