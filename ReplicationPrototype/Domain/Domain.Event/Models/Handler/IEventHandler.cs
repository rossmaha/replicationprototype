﻿namespace Replication.Domain.Event.Models
{
    public interface IEventHandler
    {
        void Handle(DomainEvent domainEvent);
    }

    public abstract class DomainEventHandler<TEvent> : IEventHandler where TEvent:DomainEvent
    {
        void IEventHandler.Handle(DomainEvent domainEvent)
        {
            Handle((TEvent) domainEvent);
        }

        public abstract void Handle(TEvent domainEvent);
    }
}
