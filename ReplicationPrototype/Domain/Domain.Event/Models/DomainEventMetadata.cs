﻿using System;
using Replication.Infastructure.Serializer;

namespace Replication.Domain.Event.Models
{
    public interface ISerializedEventMetadata
    {
        Guid AggregateId { get; }

        Type AggregateType { get; }

        Type EventType { get; }

        DateTime EventTime { get; }

        string GetSerializedEvent();
    }

    public interface IHasDomainEvent
    {
        DomainEvent GetDomainEvent();
    }

    public class DomainEventMetadata : ISerializedEventMetadata, IHasDomainEvent
    {
        private readonly Lazy<IDomainSerializer> _domainSerializer =
            new Lazy<IDomainSerializer>(DomainSerializerFactory.CreateSerializer);

        private DomainEvent _domainEvent;
        private string _serializedEvent;

        public DomainEventMetadata(DomainEvent domainEvent)
        {
            _domainEvent = domainEvent;

            AggregateId = _domainEvent.AggregateId;
            AggregateType = _domainEvent.AggregateType;
            EventType = _domainEvent.GetType();
            EventTime = _domainEvent.EventTime;
        }

        public DomainEventMetadata(Type aggregateType, Guid aggregateId, DateTime eventTime, Type eventType, string serializedEvent)
        {
            AggregateId = aggregateId;
            AggregateType = aggregateType;
            EventType = eventType;
            EventTime = eventTime;
            _serializedEvent = serializedEvent;
        }

        public Guid AggregateId { get; }

        public Type AggregateType { get; }

        public Type EventType { get; }

        public DateTime EventTime { get; }

        public string GetSerializedEvent()
        {
            return _serializedEvent ?? (_serializedEvent = _domainSerializer.Value.Serialize(_domainEvent));
        }

        public DomainEvent GetDomainEvent()
        {
            return _domainEvent ?? (_domainEvent =
                _domainSerializer.Value.Deserialize<DomainEvent>(EventType, _serializedEvent));
        }
    }
}
