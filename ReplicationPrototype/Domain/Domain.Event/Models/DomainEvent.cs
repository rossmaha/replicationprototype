﻿using System;

namespace Replication.Domain.Event.Models
{
    public abstract class DomainEvent
    {
        protected DomainEvent(Type aggregateType, Guid aggregateId, DateTime eventTime)
        {
            AggregateId = aggregateId;
            AggregateType = aggregateType;
            EventTime = eventTime;
        }

        public Guid AggregateId { get; private set; }

        public Type AggregateType { get; private set; }

        public DateTime EventTime { get; protected set; }
    }
}
