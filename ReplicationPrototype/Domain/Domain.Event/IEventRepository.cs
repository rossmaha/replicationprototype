﻿using System;
using System.Collections.Generic;
using Replication.Domain.Event.Models;

namespace Replication.Domain.Event
{
    public interface IEventRepository
    {
        IEnumerable<DomainEventMetadata> GetEventsByAggreagateId(Guid id);

        IEnumerable<DomainEventMetadata> GetEventsSinceDate(DateTime? lastUpDateTime = null);
    }
}
