﻿using System;

namespace Replication.Domain.Base
{
    public abstract class AggregateRoot
    {
        public Guid Id { get; protected internal set; }

        //public void ApplyEvents(IEnumerable<Event> eventHistory)
        //{
        //    foreach (var eventToApply in eventHistory)
        //    {
        //        dynamic d = this;

        //        d.Handle(Convert.ChangeType((dynamic)eventToApply, eventToApply.GetType()));
        //    }
        //}
    }
}
