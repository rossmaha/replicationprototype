﻿namespace Replication.Domain.Base.Events
{
    public interface IEventHandle<in TEvent> where TEvent : Event
    {
        void Handle(TEvent e);
    }
}
