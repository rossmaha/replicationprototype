﻿using System;
using System.Linq;
using Api.Net.Sync;
using Core.Sync.Dto;
using Replication.Domain.Event;
using Replication.Domain.Event.Models;
using Replication.Infastructure.CommonUtils;

namespace Replication.Services.SyncWorker.Components
{
    public interface ISyncServiceExcecutor : IDisposable
    {
        void Execute();
    }

    internal class SyncServiceExecutor : ISyncServiceExcecutor
    {
        private DateTime _lastTimeSync;

        private readonly ISyncClinet _syncClinet;
        private readonly IEventRepository _eventService;

        public SyncServiceExecutor(IEventRepository eventService, ISyncClinet syncClinet)
        {
            _eventService = eventService;
            _syncClinet = syncClinet;
            //_syncClinet = syncClient;
            //_eventRepository = unitOfWork.Events;
        }

        public void Execute()
        {
            var eventsSinceLastUpdate = _eventService.GetEventsSinceDate(_lastTimeSync).Select(ConvertEventMetaDataToDto);

            var callBackEvents = _syncClinet.GetEventsToSync(eventsSinceLastUpdate);

            foreach (var callbackEvent in callBackEvents)
            {
                
            }
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        private static EventWithMetaDataDto ConvertEventMetaDataToDto(DomainEventMetadata eventMetadata)
        {
            return new EventWithMetaDataDto
            {
                AggregateType = eventMetadata.AggregateType.ToDataString(),
                DateTime = eventMetadata.EventTime,
                AggregateId = eventMetadata.AggregateId,
                Data = eventMetadata.GetSerializedEvent(),
                EventType = eventMetadata.EventType.ToDataString()
            };
        }
    }
}
