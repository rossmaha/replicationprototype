﻿using System;
using System.Timers;
using Ninject;
using Replication.Services.SyncWorker.Components;

namespace Replication.Services.SyncWorker
{
    internal class SyncWorker : ISyncWorker
    {
        private readonly IKernel _kernel;

        private readonly Timer _timer;
        private bool _isStarted;
        private bool _isWorking;


        public SyncWorker(IKernel kernel)
        {
            _kernel = kernel;
            _timer = new Timer
            {
                AutoReset = false,
                Interval = TimeSpan.FromSeconds(60).TotalMilliseconds
            };

            _timer.Elapsed += TimerOnElapsed;

            _isStarted = false;
            _isWorking = false;
        }


        public void Start()
        {
            lock (_timer)
            {
                if (_isStarted)
                {
                    return;
                }

                _isStarted = true;

                if (!_isWorking)
                {
                    _timer.Start();
                }
            }
        }

        public void Stop()
        {
            lock (_timer)
            {
                if (!_isStarted)
                {
                    return;
                }

                _isStarted = false;

                if (!_isWorking)
                {
                    _timer.Stop();
                }
            }
        }

        private void TimerOnElapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                lock (_timer)
                {
                    _isWorking = true;
                }

                //_logger.LogInfo("Started executing sync job");

                using (var syncServiceExcecutor = _kernel.Get<ISyncServiceExcecutor>())
                {
                    syncServiceExcecutor.Execute();
                }

                //_logger.LogInfo("Finished executing sync job");
            }
            catch (Exception ex)
            {
                //_logger.LogError("Error on executing sync job", ex);
            }
            finally
            {
                lock (_timer)
                {
                    _isWorking = false;
                    if (_isStarted)
                    {
                        _timer.Start();
                    }
                }
            }
        }
    }
}
