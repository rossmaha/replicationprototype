﻿using Ninject.Modules;
using Replication.Services.SyncWorker.Components;

namespace Replication.Services.SyncWorker
{
    public class SyncServiceRegistrationModule : NinjectModule
    {
        public override void Load()
        {
            Bind<ISyncServiceExcecutor>().To<SyncServiceExecutor>();
            Bind<ISyncWorker>().To<SyncWorker>().InSingletonScope();
        }
    }
}
