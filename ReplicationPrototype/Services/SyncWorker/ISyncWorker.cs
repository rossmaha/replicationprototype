﻿namespace Replication.Services.SyncWorker
{
    public interface ISyncWorker
    {
        void Start();
    }
}
