﻿using Ninject.Modules;
using Replication.ServicesSyncService.Components;

namespace Replication.ServicesSyncService
{
    public class SyncServiceModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IConflictResolver>().To<TimeConflictResolver>();
            Bind<ISyncService>().To<SyncService>();
        }
    }
}
