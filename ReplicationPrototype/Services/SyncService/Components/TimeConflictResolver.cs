﻿using System;
using System.Collections.Generic;
using System.Linq;
using Replication.Domain.Event;
using Replication.Domain.Event.Models;

namespace Replication.ServicesSyncService.Components
{
    internal class TimeConflictResolver : IConflictResolver
    {
        public DomainChangeEvent ResolveEvents(Guid aggregateId, Type aggregateType, IEnumerable<Tuple<DomainEvent, EventSourceType>> eventsWithSource)
        {
            var resultChangeEvent = new DomainChangeEvent(aggregateId, aggregateType, DateTime.Now.ToUniversalTime());

            foreach (var eventTyple in eventsWithSource.OrderByDescending(e => e.Item1.EventTime))
            {
                var changeEvent = (DomainChangeEvent) eventTyple.Item1;

                foreach (var dictionaryItem in changeEvent.Dictionary)
                {
                    if (!resultChangeEvent.Dictionary.ContainsKey(dictionaryItem.Key))
                    {
                        resultChangeEvent.Dictionary.Add(dictionaryItem.Key, dictionaryItem.Value);
                    }
                }
            }

            return resultChangeEvent;
        }
    }
}
