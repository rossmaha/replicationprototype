﻿using System;
using System.Collections.Generic;
using Replication.Domain.Event;
using Replication.Domain.Event.Models;

namespace Replication.ServicesSyncService.Components
{
    internal enum EventSourceType
    {
        Local,
        Client
    }

    internal interface IConflictResolver
    {
        DomainChangeEvent ResolveEvents(Guid aggregateId, Type aggregateType,
            IEnumerable<Tuple<DomainEvent, EventSourceType>> eventsWithSource);
    }
}
