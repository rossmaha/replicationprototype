﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Sync.Dto;
using EventService;
using Replication.Domain.Event;
using Replication.Infastructure.CommonUtils;
using Replication.Domain.Event.Models;
using Replication.Domain.Repositories;
using Replication.ServicesSyncService.Components;

namespace Replication.ServicesSyncService
{
    internal class SyncService : ISyncService
    {
        private readonly IEventRepository _eventRepository;
        private readonly IEventHandlerService _eventHandlerService;
        private readonly IConflictResolver _conflictResolver;
        private readonly ISaveContext _saveContext;

        public SyncService(IEventRepository eventRepository, IEventHandlerService eventHandlerService,
            IConflictResolver conflictResolver, ISaveContext saveContext)
        {
            _eventRepository = eventRepository;
            _eventHandlerService = eventHandlerService;
            _conflictResolver = conflictResolver;
            _saveContext = saveContext;
        }

        public EventWithMetaDataDto[] SyncEvents(EventWithMetaDataDto[] newEventsDto, DateTime? syncTime = null)
        {
            var eventsToSend = new List<EventWithMetaDataDto>();
            var eventsToSave = new List<DomainEventMetadata>();

            var currentEvents = _eventRepository.GetEventsSinceDate(syncTime)
                .Select(e => new Tuple<DomainEventMetadata, EventSourceType>(e, EventSourceType.Local)).ToList();
            var newEvents = newEventsDto.Select(ConvertToEventWithMetaData)
                .Select(e => new Tuple<DomainEventMetadata, EventSourceType>(e, EventSourceType.Client)).ToList();

            var totalEventsQuery = currentEvents.Union(newEvents).GroupBy(e => new {e.Item1.AggregateId, e.Item1.AggregateType}, e => e);

            foreach (var eventQuery in totalEventsQuery)
            {
                var aggregateId = eventQuery.Key.AggregateId;
                var aggregateType = eventQuery.Key.AggregateType;

                var firstEventSourceType = eventQuery.First().Item2;
                var isAllFromOneSource = eventQuery.All(e => e.Item2 == firstEventSourceType);

                if (isAllFromOneSource)
                {
                    if (firstEventSourceType == EventSourceType.Local)
                    {
                        eventsToSend.AddRange(eventQuery.Select(e => ConvertEventMetaDataToDto(e.Item1)));
                    }
                    else
                    {
                        eventsToSave.AddRange(eventQuery.Select(e => e.Item1));
                    }
                }
                else
                {
                    foreach (var eventTyple in eventQuery)
                    {
                        if (eventTyple.Item2 == EventSourceType.Client)
                            eventsToSave.Add(eventTyple.Item1);
                        if (eventTyple.Item2 == EventSourceType.Local)
                            eventsToSend.Add(ConvertEventMetaDataToDto(eventTyple.Item1));
                    }

                    var resolvedEvent = _conflictResolver.ResolveEvents(aggregateId, aggregateType, eventQuery.Select(e =>
                        new Tuple<DomainEvent, EventSourceType>(e.Item1.GetDomainEvent(), e.Item2)));

                    var domainEventMetaData = new DomainEventMetadata(resolvedEvent);

                    eventsToSend.Add(ConvertEventMetaDataToDto(domainEventMetaData));
                    eventsToSave.Add(domainEventMetaData);
                }

                if (eventsToSave.Any())
                {
                    _eventHandlerService.ApplyEvents(eventQuery.Key.AggregateType, eventQuery.Key.AggregateId, eventsToSave);

                    try
                    {
                        _saveContext.Save();
                    }
                    catch (Exception)
                    {
                        // TODO RESTART ON UPDATE ERROR
                    }
                }
            }

            return eventsToSend.ToArray();
        }

        private static EventWithMetaDataDto ConvertEventMetaDataToDto(ISerializedEventMetadata eventMetadata)
        {
            return new EventWithMetaDataDto
            {
                AggregateType = eventMetadata.AggregateType.ToDataString(),
                DateTime = eventMetadata.EventTime,
                AggregateId = eventMetadata.AggregateId,
                Data = eventMetadata.GetSerializedEvent(),
                EventType = eventMetadata.EventType.ToDataString()
            };
        }

        private static DomainEventMetadata ConvertToEventWithMetaData(EventWithMetaDataDto eventDto)
        {
            return new DomainEventMetadata(
                Type.GetType(eventDto.AggregateType),
                eventDto.AggregateId,
                eventDto.DateTime,
                Type.GetType(eventDto.EventType),
                eventDto.Data);
        }
    }
}