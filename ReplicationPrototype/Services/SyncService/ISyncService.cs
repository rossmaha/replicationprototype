﻿using System;
using Core.Sync.Dto;

namespace Replication.ServicesSyncService
{
    public interface ISyncService
    {
        EventWithMetaDataDto[] SyncEvents(EventWithMetaDataDto[] newEvents, DateTime? syncTime = null);
    }
}
