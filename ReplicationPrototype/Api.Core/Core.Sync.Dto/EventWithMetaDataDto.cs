﻿using System;

namespace Core.Sync.Dto
{
    public class EventWithMetaDataDto
    {
        //TODO ADD TO TYPE CONVERTER
        public string AggregateType { get; set; }

        public Guid AggregateId { get; set; }

        public DateTime DateTime { get; set; }

        //TODO ADD TO TYPE CONVERTER
        public string EventType { get; set; }

        public string Data { get; set; }
    }
}
