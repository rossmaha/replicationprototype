﻿using System;
using System.Web.Http;
using Core.Sync.Dto;
using Replication.ServicesSyncService;

namespace ReplicationPrototype.Web.Controllers
{
    [RoutePrefix("api/Sync")]
    public class SyncController : ApiController
    {
        private readonly ISyncService _syncService;
        
        public SyncController(ISyncService syncService)
        {
            _syncService = syncService;
        }

        [Route("SyncEvents")]
        [HttpPost]
        public IHttpActionResult SyncEvents([FromBody]EventWithMetaDataDto[] eventsToSync, DateTime? syncTime = null)
        {
            return Ok(_syncService.SyncEvents(eventsToSync, syncTime));
        }
    }
}