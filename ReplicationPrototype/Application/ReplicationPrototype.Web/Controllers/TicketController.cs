﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Replication.Domain.Models;
using ReplicationPrototype.Web.Models;
using ReplicationPrototype.Web.Services;

namespace ReplicationPrototype.Web.Controllers
{
    public class TicketController : ApiController
    {
        private readonly ITicketService _ticketService;

        public TicketController(ITicketService ticketService)
        {
            _ticketService = ticketService;
        }

        [Route]
        [HttpPost]
        public IHttpActionResult Create(CreateTicketViewModel createIncidentCommand)
        {
            _ticketService.CreateTicket(createIncidentCommand);

            return Ok();
        }

        [Route("{id}")]
        [HttpPut]
        public IHttpActionResult Update(Guid id, CreateIncidentViewModel changeIncidentViewModel)
        {
            _incidenService.ChangeIncident(id, changeIncidentViewModel);

            return Ok();
        }

        [Route("{id}")]
        [HttpGet]
        public Incident GetIncidentFromEvents(Guid id)
        {
            return _incidenService.GetIncidentById(id);
        }
    }
}