﻿using System;
using System.Web.Http;
using Replication.Domain.Models;
using ReplicationPrototype.Web.Models;
using ReplicationPrototype.Web.Services;

namespace ReplicationPrototype.Web.Controllers
{
    [RoutePrefix("api/Incidents")]
    public class IncidentController: ApiController
    {
        private readonly IIncidenService _incidenService;

        public IncidentController(IIncidenService incidenService)
        {
            _incidenService = incidenService;
        }

        [Route]
        [HttpPost]
        public IHttpActionResult Create(CreateIncidentViewModel createIncidentCommand)
        {
            _incidenService.CreateIncident(createIncidentCommand);

            return Ok();
        }

        [Route("{id}")]
        [HttpPut]
        public IHttpActionResult Update(Guid id, CreateIncidentViewModel changeIncidentViewModel)
        {
            _incidenService.ChangeIncident(id, changeIncidentViewModel);

            return Ok();
        }

        [Route("{id}")]
        [HttpGet]
        public Incident GetIncidentFromEvents(Guid id)
        {
            return _incidenService.GetIncidentById(id);
        }
    }
}