﻿using System;
using Replication.Domain.Models;
using Replication.Domain.Repositories;
using ReplicationPrototype.Web.Models;

namespace ReplicationPrototype.Web.Services
{
    public interface IIncidenService
    {
        void CreateIncident(CreateIncidentViewModel createIncidentCommand);

        void ChangeIncident(Guid id, CreateIncidentViewModel changeIncidentViewModel);

        Incident GetIncidentById(Guid id);
    }

    public class IncidentService : IIncidenService
    {
        private readonly IIncidentRepository _incidentRepository;
        private readonly ISaveContext _contextSaver;

        public IncidentService(IUnitOfWork repository)
        {
            _incidentRepository = repository.Incidents;
            _contextSaver = repository;
        }

        public void CreateIncident(CreateIncidentViewModel createIncidentCommand)
        {
            var incident = new Incident(Guid.NewGuid(), createIncidentCommand.Name)
            {
                Description = createIncidentCommand.Description
            };

            _incidentRepository.Add(incident);

            _contextSaver.Save();
        }

        public void ChangeIncident(Guid id, CreateIncidentViewModel changeIncidentViewModel)
        {
            var incident = _incidentRepository.GetById(id);
            incident.Name = changeIncidentViewModel.Name;
            incident.Description = changeIncidentViewModel.Description;

            _incidentRepository.Update(incident);
            _contextSaver.Save();
        }

        public Incident GetIncidentById(Guid id)
        {
            return _incidentRepository.GetById(id);
        }

        //public Incident GetIncidentFromEvents(Guid id)
        //{
        //    Incident result = null;
        //    var eventsToApply = _eventRepository.GetEventsByAggreagateId(id).ToList();

        //    if (eventsToApply.Any())
        //    {
        //        result = new Incident(Guid.Empty, null);
        //        result.ApplyEvents(eventsToApply);
        //    }

        //    return result;
        //}
    }
}