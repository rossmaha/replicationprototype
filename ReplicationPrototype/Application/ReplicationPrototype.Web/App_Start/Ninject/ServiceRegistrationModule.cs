﻿using Ninject.Modules;
using Ninject.Web.Common;
using ReplicationPrototype.Web.Services;

namespace ReplicationPrototype.Web.Ninject
{
    public class ServiceRegistrationModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IIncidenService>().To<IncidentService>().InRequestScope();
        }
    }
}