using System;
using System.Web;
using System.Web.Http;
using EventService;
using EventService.Components;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using Ninject;
using Ninject.Extensions.Factory;
using Ninject.Web.Common;
using Replication.Domain.Data;
using Replication.Domain.Event.Models;
using Replication.Domain.Models;
using Replication.ServicesSyncService;
using ReplicationPrototype.Web.Ninject;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(NinjectConfig), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethod(typeof(NinjectConfig), "Stop")]

namespace ReplicationPrototype.Web.Ninject
{
    public static class NinjectConfig 
    {
        private static readonly Bootstrapper _bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            _bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            _bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                GlobalConfiguration.Configuration.DependencyResolver = new NinjectResolver(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            //kernel.Bind<IncidentCreatedEventHandler>().ToSelf();
            //kernel.Bind<IEventHandler<IncidentCreatedEvent>>().To<IncidentCreatedEventHandler>();
            //kernel.Load("Domain.Data.dll");
            kernel.Load(new DomainDataModule());
            kernel.Load(new EventServiceRegistrationModule());
            kernel.Load(new ServiceRegistrationModule());
            kernel.Load(new SyncServiceModule());


            //kernel.Load(new EventServiceRegistrationModule());
        }
    }
}
