﻿namespace ReplicationPrototype.Web.Models
{
    public class CreateIncidentViewModel
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}