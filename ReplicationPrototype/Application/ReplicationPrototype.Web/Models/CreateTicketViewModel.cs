﻿using System;

namespace ReplicationPrototype.Web.Models
{
    public class CreateTicketViewModel
    {
        public Guid IncidentId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
    }
}