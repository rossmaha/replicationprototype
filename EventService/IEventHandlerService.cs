﻿using System;
using System.Collections.Generic;
using Replication.Domain.Event.Models;

namespace EventService
{
    public interface IEventHandlerService
    {
        void ApplyEvents(Type aggregateType, Guid aggregateId, IEnumerable<DomainEventMetadata> events);
    }
}
