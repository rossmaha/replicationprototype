﻿using System;
using System.Collections.Generic;
using EventService.Components;
using Replication.Domain.Data.Models;
using Replication.Domain.Event.Models;

namespace EventService
{
    public class EventHandlerService : IEventHandlerService
    {
        private readonly IEventHandlerFactory _eventHandlerFactory;

        public EventHandlerService(IEventHandlerFactory eventHandlerFactory)
        {
            _eventHandlerFactory = eventHandlerFactory;
        }

        public void ApplyEvents(Type aggregateType, Guid aggregateId, IEnumerable<DomainEventMetadata> events)
        {
            foreach (var @event in events)
            { 
                HandleEvent(ChangeTo(@event.GetDomainEvent(), @event.EventType));
            }
        }
        private void HandleEvent<T>(T domainEvent) where T : DomainEvent
        {
            var handler = _eventHandlerFactory.GetHandler<T>();
            handler.Handle(domainEvent);
        }

        private static dynamic ChangeTo(dynamic source, Type dest)
        {
            return Convert.ChangeType(source, dest);
        }
    }
}
