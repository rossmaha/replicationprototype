﻿using System;
using EventService.Components;
using Ninject.Extensions.Factory;
using Ninject.Modules;
using Replication.Domain.Event.Models;
using Replication.Domain.Models;

namespace EventService
{
    public class EventServiceRegistrationModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IEventHandlerFactory>().To<EventHandlerFactory>();
            Bind<IEventHandlerService>().To<EventHandlerService>();


            //https://github.com/ninject/ninject.extensions.conventions extension.
            //Bind(scanner => scanner.FromAssemblyContaining<GetEngineOptionsRequestHandle‌​r>().SelectAllClasse‌​s().InheritedFromAny‌​(new[] { typeof(IEventHandler<>)) }).BindAllInterfaces().Configure(c‌​ => c.InTransientScope‌​()));

            //Bind<IEventHandler<IncidentCreatedEvent>>().To<IncidentCreatedEventHandler>();
            //Bind<IEventHandlerFactory>().ToFactory();
            //Bind<IEventService>().To<EventService>();
        }
    }
}
