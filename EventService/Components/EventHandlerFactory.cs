﻿using System;
using Replication.Domain.Event.Models;
using Replication.Domain.Models;
using Replication.Domain.Repositories;

namespace EventService.Components
{
    public class EventHandlerFactory : IEventHandlerFactory
    {
        private readonly IUnitOfWork _unitOfWork;

        public EventHandlerFactory(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEventHandler GetHandler<TRequest>() where TRequest : DomainEvent
        {
            var type = typeof(TRequest);

            if (type == typeof(IncidentCreatedEvent))
                return new IncidentCreatedEventHandler(_unitOfWork);

            throw new Exception();
        }
    }
}
