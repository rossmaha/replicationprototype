﻿using Replication.Domain.Event.Models;


namespace EventService.Components
{
    public interface IEventHandlerFactory
    {
        IEventHandler GetHandler<TRequest>() where TRequest : DomainEvent;
    }
}
